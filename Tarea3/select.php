<html>
    <head>
        <title>Registro alumnos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    </head>
    <body>
    <div class="container">
		<div class="columns">
        <div class="column col-2"></div>
        <div class="column col-8">
        <h1>Registros de alumnos</h1>
        <a href="formulario2.php"><i class="icon icon-2x icon-people"></i> Agregar</a>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Número de cuenta</th>
                    <th>Nombre</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Género</th>
                    <th>Fecha de Nacimiento</th>
                </tr>
            </thead>
            <tbody>        
                <?php
                require_once('./conn.php');
                try {
                    // FETCH_OBJ
                    $stmt = $dbh->prepare("SELECT * FROM alumno");
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                    if (!empty($result)) {
                        foreach($result as $row) {
                            echo <<<EOL
                                <tr>
                                    <td>{$row->alumno_id}</td>
                                    <td>{$row->al_numcta}</td>
                                    <td>{$row->al_nombre}</td>
                                    <td>{$row->al_apellido1}</td>
                                    <td>{$row->al_apellido2}</td>
                                    <td>{$row->al_genero}</td>
                                    <td>{$row->al_fechaNac}</td>
                                </tr>
                            EOL;
                        }
                    } else {
                        echo "<tr><td colspan='4'>No hay datos para mostrar</td></tr>";
                    }
                } catch (Exception $e) {
                    echo $e->getMessage();
                } finally {
                    $dbh = null;
                }
                ?>
            </tbody>
            </table>
        </div>
        </div>
    </div>
    </body>
</html>
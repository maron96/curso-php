<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Formulario
	</title>
</head>
<body>
	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Formulario</h1></header><br>
	<form method="post">
		<div class="form-group row">

			<label class="col-sm-3" for="CajaTexto1">Número de cuenta:</label>
			<div class="col-sm-4">
					<input class="form-control" type="text" name="numerocuenta" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>
			<label class="col-sm-3" for="CajaTexto2">Nombre:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="nombre" id="CajaTexto2">
			</div>
			<div class="col-sm-4">
			</div>
			<label class="col-sm-3" for="CajaTexto3">Primer apellido:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="primerapellido" id="CajaTexto3">
			</div>
			<div class="col-sm-4">
			</div>
            <label class="col-sm-3" for="CajaTexto3">Segundo apellido:</label>
			<div class="col-sm-4">
				<input class="form-control" type="text" name="segundoapellido" id="CajaTexto4">
			</div>
			<div class="col-sm-4">
			</div>
		</div>
        <fieldset class="form-group">
            <label>Genero:</label>
            
            <div class="form-check">
                <label class="form-check-label">
                <input type="radio" class="form-check-input" name="genero" id="optionsRadios1" value="M" checked>
                Masculino
                </label>
            </div>

            <div class="form-check disabled">
                </label>
                <label class="form-check-label">
                <input type="radio" class="form-check-input" name="genero" id="optionsRadios2" value="F">
                Femenino
                </label>
            </div>

            <div class="form-check disabled">
                <label class="form-check-label">
                <input type="radio" class="form-check-input" name="genero" id="optionsRadios3" value="Otro">
                 Otro
                </label>
            </div>
        </fieldset>
        <div class="form-group row">

            <label class="col-sm-3" for="CajaTexto3">Contraseña:</label>
			<div class="col-sm-4">
				<input class="form-control" type="password" name="password" id="CajaPassword1">
			</div>
			<div class="col-sm-4">
			</div>

            <label class="col-sm-3" for="CajaTexto3">Fecha de nacimiento:</label>
			<div class="col-sm-4">
				<input class="form-control" type="date" name="fechanacimiento" id="CajaFecha1">
			</div>
			<div class="col-sm-4">
			</div>

        </div>

		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="./select.php">Ver registros</a>
	</form>
	</div>
</body>
</html>
<?php
require_once("./insert.php");
?>
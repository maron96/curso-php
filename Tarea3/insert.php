<?php
try {
    require_once("./conn.php");
    $stmt = $dbh->prepare("INSERT INTO alumno(al_numcta, al_nombre, al_apellido1, al_apellido2, al_genero, al_fechaNac)
    VALUES (:numerocuenta, :nombre, :ap1, :ap2, :genero, :fechaNac)");
    $stmt->bindParam(":nombre",$_POST['nombre']);
    $stmt->bindParam(":numerocuenta",$_POST['numerocuenta']);
    $stmt->bindParam(":ap1",$_POST['primerapellido']);
    $stmt->bindParam(":ap2",$_POST['segundoapellido']);
    $stmt->bindParam(":genero",$_POST['genero']);
    $stmt->bindParam(":ap1",$_POST['primerapellido']);
    $stmt->bindParam(":fechaNac",$_POST['fechanacimiento']);
    $stmt->execute();
    echo '<input type="text" class="form-control" value="Inserción realizada" readonly>';
}
catch (Exception $e) {
    
}
finally {
    $dbh=null;
}
?>
<?php
include_once('transporte.php');
class bicicleta extends transporte{
    private $rodada;
    public function __construct($nom,$vel,$com,$rod){
        parent::__construct($nom,$vel,$com);
        $this->rodada=$rod;
    }

    public function resumenBicicleta(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Rodada:</td>
                    <td>'. $this->rodada.'</td>				
                </tr>';
        return $mensaje;
    }
}
?>
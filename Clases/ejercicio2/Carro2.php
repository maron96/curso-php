<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $verificacion;

	//declaracion del método verificación
	public function verificacion($añoFabricacion){
		if($añoFabricacion < 1990){
			$this->verificacion = "NO";
		}
		elseif(($añoFabricacion >= 1990) and ($añoFabricacion < 2010)){
			$this->verificacion = "REVISION";
		}
		else{
			$this->verificacion = "SI";
		}
	}

	//getter
	public function get_verificacion(){
		return $this->verificacion;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->verificacion((int)$_POST['añoFabricacion']);
}





<?php
// Email
$esEmail='/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/';
$mail=preg_match($esEmail,"jorge@gmail.com");
echo $mail;

// CURP
$esCurp='/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/';
$curp=preg_match($esCurp,"TESM960903HDFRRR05");
echo $curp;

// Palabras
$espalabra50='/[A-z]{50,}/';
$palabra50=preg_match($espalabra50,"ASBdsgagdhASBdsgagdhASBdsgagdhASBdsgagdhASBdsgagdhASBdsgagdh");
echo $palabra50;

// Numero decimales
$esDecimal='/[0-9][\.][0-9]/';
$decimal=preg_match($esDecimal,"10.1");
echo $decimal;

// Funcion para escapar caracteres especiales

function escaparEspeciales ($cadena){
    preg_quote($cadena);
}
?>
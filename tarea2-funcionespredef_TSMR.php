<?php
 
/* 1. substr()
Es una función que recibe como parámetros una cadena, un inicio y un final
para devolver una subcadena de la cadena dado el rango que se quiere.
*/

// Ejemplo:
$cadena1="Soy un pedacito completo";
$subCadena1=substr($cadena1,0,15);
echo $subCadena1."\n";

/* 2. strstr()
Es un función que devuelve un string de acuerdo a string2,
por defecto devuelve el resto del string apartir de la primer 
ocurrencia del string2, si se le pasa además el parámetro true
devolvera la parte del string antes de la ocurrencia de string2
y sin incluirlo. Es sentivo a mayusculas y minusculas.
*/

// Ejemplo:

$cadena2="AntesYDespués";
$subcadena2defecto=strstr($cadena2,"Y");
echo $subcadena2defecto."\n";
$subcadena2True=strstr($cadena2,"Y", true);
echo $subcadena2True."\n";

/* 3. strpos() 
Devuelve la posicion en la que se encuentra la primer ocurrencia de 
un caracter, por defecto empieza en la posicion 0 del string, pero 
se puede especificar que comience a buscar desde otra posicion.
Recibe un string en el cual buscar, un string a buscar y puede 
recibir la posicion inicial para buscar.
*/

// Ejemplo:

$cadena3="EjemploXEjemploX";
$posX=strpos($cadena3,"X");
echo "La primer posición de X en \"".$cadena3."\" es ".$posX."\n";
$posX2=strpos($cadena3,"X",($posX+1));
echo "La segunda posición de X en \"".$cadena3."\" es ".$posX2."\n";

/* 4. implode() 
Es una función que sirve para unir los elementos de un array en un
string. Recibe como parámetros un string para unir y el array a unir,
por defecto el string para unir es uno vacío.
*/

// Ejemplo: 

$array1 = array("Soy","un","array");
$cadenaDeArray1 = implode($array1);
echo $cadenaDeArray1."\n";
echo implode(" ",$array1)."\n";

/* 5. explode() 
Devuelve un array de string, siendo cada uno un substring del parámetro string formado 
por la división realizada por los delimitadores indicados en el parámetro delimiter.
Puede recibir como parámetro un limitador (por defecto es el máximo de elementos), tal que:

Si el parámetro limit es positivo, el array devuelto contendrá el máximo de limit elementos, 
y el último elemento contendrá el resto del string.

Si el parámetro limit es negativo, se devolverán todos los componentes a excepción del último -limit.

Si el parámetro limit es cero,se tratará como 1.
*/

// Ejemplo:

$cadena4="1.2.3.4.5";
$arrayDeC4=explode(".",$cadena4); // cada elemento será un número
echo implode(" ",$arrayDeC4)."\n";

$arrayDeC4=explode(".",$cadena4,3); // cada elemento será un número hasta el tercer elemento que incluirá el resto del string
echo implode(" ",$arrayDeC4)."\n";

$arrayDeC4=explode(".",$cadena4,-1); // cada elemento será un número, menos los elementos indicados apartir del último
echo implode(" ",$arrayDeC4)."\n";

$arrayDeC4=explode(".",$cadena4,0); // un único elemento en el array
echo implode($arrayDeC4)."\n";

/* 6. y 7. utf8_encode() y utf8_decode()

Son funciones que reciben un string en codificación UTF-8 (UNICODE) o ISO-8859-1 respectivamente
y lo convierte a la codificación contraria, devuelve el string en la codificación correspondiente.
*/

// Ejemplos:

echo utf8_encode("Mañana toca programación"."\n"); // devuelve "MaÃ±ana toca programaciÃ³n"
echo utf8_decode("MaÃ±ana toca programaciÃ³n"."\n"); // devuelve "Mañana toca programación"

/* 8. array_pop()
Devuelve el último valor del array. Si el array está vacío, se devolverá NULL. 
Acortará el array con un elemento menos. */

// Ejemplo:

$arrayCoches=array("Mustang","Camaro","Viper","350Z");
$autojapones=array_pop($arrayCoches);
echo implode(" ",$arrayCoches)."\n";

/* 9. array_push()

Estructura: array_push ( array &$array , mixed $value1 [, mixed $... ] ) : int

Trata al array como si fuera una pila y coloca la variable que se le proporciona al final del 
array. El tamaño del array será incrementado por el número de variables insertados. Se recomienda
utilizarla cuando se agregará más de un valor. Devuelve el número de elementos en el array.
*/

// Ejemplo:

$cantNuevosElementos= array_push($arrayCoches,"Challenger","Covertte");

echo "El array de coches ahora tiene ".$cantNuevosElementos." elementos."."\n";
echo implode(" ",$arrayCoches)."\n";

/* 10. array_diff()

Estructura:

array_diff ( array $array1 , array $array2 [, array $... ] ) : array

Compara array1 con uno o más arrays y devuelve los valores de array1 que no estén presentes en ninguno 
de los otros arrays. Los resultados los devuelve en otro array.*/


//Ejemplo:

$frutas=array("Manzana","Platano","Durazno","Pera","Mandarina","Sandia");
$frutasYverduras=array("Manzana","Durazno","Ejote","Espinaca","Brocoli","Colifor");
$frutasYpastas=array("Platano","Mandarina","Espagueti","Sopa");
$frutasSobrantes=array_diff($frutas,$frutasYverduras,$frutasYpastas);
echo implode(" ", $frutasSobrantes)."\n";

/* 11. array_walk()
Estructura:
array_walk ( array &$array , callable $callback [, mixed $userdata = NULL ] ) : bool

Aplica la función definida por el usuario dada por callback a cada elemento del array dado por array.
Devuelve true si todo se ejecuto como debía y false en caso contrario.

La funcion pasada debe aceptar valor y llave como parámetro.
*/

// Ejemplo:

function mayusculas(&$value,&$key){
    $value=strtoupper($value);
};

array_walk($frutasSobrantes,'mayusculas'); 
echo implode(" ", $frutasSobrantes)."\n";

/* 12. sort()

Estructura:
sort ( array &$array [, int $sort_flags = SORT_REGULAR ] ) : bool
El segundo valor puede cambiar el tipo ordenamiento de acuerdo a un
rango de constantes. Por defecto compara sin hacer cambio de tipos.
Devuelve true en caso de éxito, false en caso de fallo. 
*/

// Ejemplo:

$array3=array('z','y','g','a','w','b','j');
sort($array3);
echo implode(" ", $array3)."\n";

/*13. current()
Devuelve el elemento actual en un array. Recibe un arrary.
 */

 // Ejemplo:

$transport = array('pie', 'bici', 'coche', 'avión');
$elementoactual = current($transport); 
echo $elementoactual."\n";
$elementosiguiente = next($transport);
echo $elementosiguiente."\n";    
$elementoactual= current($transport); //el puntero apunta al siguiente elemento
echo $elementoactual."\n";

/* 14. date() 
date ( string $format [, int $timestamp = time() ] ) : string
Devuelve una cadena formateada según el formato dado usando el parámetro de tipo integer timestamp dado 
o el momento actual si no se da una marca de tiempo. En otras palabras, timestamp es opcional y por 
defecto es el valor de time().
La correspondencia entre valor del string y formato devuelvo puede consultarse en:
https://www.php.net/manual/es/function.date.php
*/

// Ejemplo:

$fecha=date("D d m o H:i:s A");
echo $fecha;

/* 15 y 16, isset() y empty()

empty: Determina si una variable es considerada vacía. Devuelve false si la varible está vacía o true si no.
"" (una cadena vacía)
0 (0 como un integer)
0.0 (0 como un float)
"0" (0 como un string)
NULL
FALSE
array() (un array vacío)
$var; (una variable declarada, pero sin un valor)

isset: Determina si una variable está definida y no es NULL
devuelve true si existe o false si no
*/

// Ejemplos:

$variable="";
isset($variable); // True
empty($variable); // False

/* 
17 y 18; serialize() y unserialize
Estructura:
serialize ( mixed $value ) : string

Genera una representación almacenable de un valor.
Esto es útil para el almacenamiento de valores en PHP sin perder su tipo y estructura.
Para recuperar el valor PHP a partir de la cadena seriada, utilice unserialize().

Suele utilizarce para almacenar objetos complejos en representación binaria dentro de bases
de datos relacionales utilizando BLOB.
*/

// Sin ejemplo propio. El manal de php utiliza el siguiente

/*
// $datos_sesion contiene un array multi-dimensional con
// información del usuario actual. Usamos serialize() para
// almacenarla en una base de datos al final de la petición.

$con  = odbc_connect("bd_web", "php", "gallina");
$sent = odbc_prepare($con,
      "UPDATE sesiones SET datos = ? WHERE id = ?");
$datos_sql = array (serialize($datos_sesion), $_SERVER['PHP_AUTH_USER']);

if (!odbc_execute($sent, &$datos_sql)) {
    $sent = odbc_prepare($con,
     "INSERT INTO sesiones (id, datos) VALUES(?, ?)");
    if (!odbc_execute($sent, &$datos_sql)) {
         // Algo ha fallado.. 
    }
*/
?>